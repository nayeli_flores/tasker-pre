-- Crear la base de datos
CREATE DATABASE IF NOT EXISTS administrador_tareas;
USE administrador_tareas;

-- Crear la tabla Usuario
CREATE TABLE IF NOT EXISTS Usuario (
    id INT AUTO_INCREMENT PRIMARY KEY,
    username VARCHAR(255) NOT NULL,
    contrasena VARCHAR(255) NOT NULL,
    nombre VARCHAR(255) NOT NULL
);

-- Crear la tabla Tarea
CREATE TABLE IF NOT EXISTS Tarea (
    id INT AUTO_INCREMENT PRIMARY KEY,
    titulo VARCHAR(255) NOT NULL,
    descripcion TEXT,
    status ENUM('por-hacer', 'en-progreso', 'completada') NOT NULL,
    usuario_id INT,
    FOREIGN KEY (usuario_id) REFERENCES Usuario(id)
);

-- Insertar usuarios en la tabla Usuario
INSERT INTO Usuario (username, contrasena, nombre) VALUES
    ('administrador', 'admin', 'Administrador'),
    ('john_doe', 'john', 'John Doe');

