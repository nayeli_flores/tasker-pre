<?php

require "utilidades\clases\ConexionDB.php";

class Usuario {
    public $id;
    public $username;
    private $contrasena;
    public $nombre;

    public function __construct($username = null, $contrasena = null, $nombre = null, $id = null) {
        $this->username = $username;
        $this->contrasena = $contrasena;
        $this->nombre = $nombre;
    }

    public function iniciarSesion() {
        $conexionDB = new ConexionDB();
        $pdo = $conexionDB->getConnection();
        
        // Preparar la consulta SQL
        $query = "SELECT * FROM Usuario WHERE username = :username AND contrasena = :contrasena";
        $statement = $pdo->prepare($query);

        $statement->bindParam(':username', $this->username, PDO::PARAM_STR);
        $statement->bindParam(':contrasena', $this->contrasena, PDO::PARAM_STR);

        // Ejecutar la consulta
        $statement->execute();

        // Obtener los resultados
        $usuario = $statement->fetch(PDO::FETCH_ASSOC);

        $conexionDB->__destruct();

        if($usuario) {
            $this->id = $usuario['id'];
            $this->username = $usuario['username'];
            $this->nombre = $usuario['nombre'];
            $this->contrasena = $usuario['contrasena'];
            $this->guardarEnSesion();
            return true;
        }
        return false;
    }

    private function guardarEnSesion() {
        session_start();
        $_SESSION['id'] = $this->id;
        $_SESSION['username'] = $this->username;
        $_SESSION['nombre'] = $this->nombre;
    }

    public function obtenerSesion() {
        session_start();
        if (isset($_SESSION)) {
            return new Usuario($_SESSION['username'], null, $_SESSION['nombre'], $_SESSION['id']);
        }
        return false;
    }

    public function cerrarSesion() {
        session_destroy();
    }

    public function obtenerTodos() {
        $conexionDB = new ConexionDB();
        $pdo = $conexionDB->getConnection();
        
        // Preparar la consulta SQL
        $query = "SELECT * FROM Usuario";
        $statement = $pdo->prepare($query);

        // Ejecutar la consulta
        $statement->execute();

        // Obtener los resultados
        $usuarios = $statement->fetchAll(PDO::FETCH_ASSOC);

        $conexionDB->__destruct();

        // Mostrar los resultados
        $listaUsuarios = [];
        foreach ($usuarios as $usuario) {
            array_push(
                $listaUsuarios, 
                new Usuario(
                    $usuario['username'], 
                    $usuario['contrasena'], 
                    $usuario['nombre'], 
                    $usuario['id'])
                );
        }
        return $listaUsuarios;
    }
}