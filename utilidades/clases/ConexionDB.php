<?php

class ConexionDB {
    private $host = "localhost";
    private $username = "root";
    private $password = "";
    private $database = "administrador_tareas";
    private $conn;

    // Constructor
    public function __construct() {
        // Establecer la conexión
        try {
            $dsn = "mysql:host={$this->host};dbname={$this->database}";
            $this->conn = new PDO($dsn, $this->username, $this->password);
            // Habilitar el manejo de errores de PDO
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            die("Error de conexión: " . $e->getMessage());
        }
    }

    // Obtener la conexión
    public function getConnection() {
        return $this->conn;
    }

    // Cerrar la conexión al destruir el objeto
    public function __destruct() {
        $this->conn = null;
    }
}

